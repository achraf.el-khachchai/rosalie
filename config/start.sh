#!/bin/sh
set -x

root_dir="$(realpath "$(dirname "$0")/.." )"
config_file="$root_dir/config/nginx.conf"

ensuredir() {
    if [ ! -e "$1" ]; then
        mkdir "$1"
    fi
}

ensurevar() {
    if [ -z "$(eval "echo \$$1")" ]; then
        eval "export $1=$2"
    fi
}

ensuredir "$root_dir/temp"
ensuredir "$root_dir/logs"

ensurevar FRONTEND_PORT 3000
ensurevar BACKEND_PORT 4000
ensurevar PORT 8080


envsubst < "${config_file}.template" > "$config_file"

exec nginx -c "$config_file" -p "$root_dir" "$@"
