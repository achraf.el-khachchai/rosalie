module Main exposing (..)

import Bootstrap.Navbar as Navbar
import Data.Edition exposing (Edition)
import Html exposing (Html, div, h1, header, img, text)
import Html.Attributes exposing (alt, class, height, href, src, width)
import Navigation exposing (Location)
import Page.Application.Candidates as AppC
import Page.Edition.Encounters as Enc
import Page.Edition.Retrieve as EditionR
import Page.Movie.Article as MovA
import Page.Movie.Detail as MovD
import Page.News.Feed as Feed
import Page.News.Latest as Latest
import Page.News.Read as Read
import Page.Participate as Par
import Page.Us exposing (us)
import Page.Wrap exposing (wrap)
import RemoteData exposing (WebData)
import Request.Edition as EditionR
import Route exposing (Origin, Route, parseLocation)
import Util exposing ((=>))


---- MODEL ----


type Page
    = NewsLatest Latest.Model
    | NewsFeed Feed.Model
    | News Read.Model
    | Us
    | Edition EditionR.Model
    | Candidates AppC.Model
    | Encounters Enc.Model
    | Mov MovD.Model
    | Article MovA.Model
    | Participate Par.Model
    | NotFound


type alias Model =
    { page : Page
    , location : Location
    , navstate : Navbar.State
    , editions : WebData (List Edition)
    , flags : Flags
    }


type alias Flags =
    { logo : String
    , gitIcon : String
    }


init : Flags -> Location -> ( Model, Cmd Msg )
init flags location =
    let
        ( navstate, navcmd ) =
            Navbar.initialState NavMsg

        page =
            locationPage location
    in
    Model page location navstate RemoteData.Loading flags
        ! [ locationMsg location, navcmd, listEditions ]


locationPage : Location -> Page
locationPage location =
    let
        route =
            parseLocation location
    in
    case route of
        Route.NewsLatest ->
            NewsLatest (Tuple.first Latest.init)

        Route.NewsFeed ->
            NewsFeed (Tuple.first Feed.init)

        Route.News nid ->
            News (Tuple.first (Read.init nid))

        Route.Edition eid ->
            Edition (Tuple.first (EditionR.init eid (Route.locationOrigin location)))

        Route.Candidates eid ->
            Candidates (Tuple.first (AppC.init eid))

        Route.Movie mid ->
            Mov (Tuple.first (MovD.init mid))

        Route.Encounters ->
            Encounters (Tuple.first Enc.init)

        Route.Article i ->
            Article (Tuple.first (MovA.init i))

        Route.Participate eid ->
            Participate (Tuple.first (Par.init eid))

        Route.Us ->
            Us

        Route.NotFound ->
            NotFound


locationMsg : Location -> Cmd Msg
locationMsg location =
    let
        route =
            parseLocation location
    in
    case route of
        Route.NewsLatest ->
            Cmd.map NewsLatestMsg (Tuple.second Latest.init)

        Route.NewsFeed ->
            Cmd.map NewsFeedMsg (Tuple.second Feed.init)

        Route.News nid ->
            Cmd.map NewsMsg (Tuple.second (Read.init nid))

        Route.Edition eid ->
            Cmd.map EditionMsg (Tuple.second (EditionR.init eid (Route.locationOrigin location)))

        Route.Candidates eid ->
            Cmd.map CandidatesMsg (Tuple.second (AppC.init eid))

        Route.Movie mid ->
            Cmd.map MovMsg (Tuple.second (MovD.init mid))

        Route.Encounters ->
            Cmd.map EncountersMsg (Tuple.second Enc.init)

        Route.Article i ->
            Cmd.map ArticleMsg (Tuple.second (MovA.init i))

        Route.Participate eid ->
            Cmd.map ParticipateMsg (Tuple.second (Par.init eid))

        _ ->
            Cmd.none



---- UPDATE ----


type Msg
    = LocationChange Location
    | NavMsg Navbar.State
    | ListEditions (WebData (List Edition))
    | NewsLatestMsg Latest.Msg
    | NewsFeedMsg Feed.Msg
    | NewsMsg Read.Msg
    | EditionMsg EditionR.Msg
    | CandidatesMsg AppC.Msg
    | EncountersMsg Enc.Msg
    | MovMsg MovD.Msg
    | ArticleMsg MovA.Msg
    | ParticipateMsg Par.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LocationChange location ->
            let
                page =
                    locationPage location
            in
            { model | page = page, location = location }
                => locationMsg location

        NavMsg state ->
            { model | navstate = state } ! []

        ListEditions editions ->
            { model | editions = editions } ! []

        _ ->
            updatePage model.page msg model


updatePage : Page -> Msg -> Model -> ( Model, Cmd Msg )
updatePage page msg model =
    let
        toPage toUpdate subMsg subModel toModel toMsg =
            let
                ( newModel, newCmd ) =
                    toUpdate subMsg subModel
            in
            { model | page = toModel newModel } => Cmd.map toMsg newCmd
    in
    case ( page, msg ) of
        ( NewsLatest subModel, NewsLatestMsg subMsg ) ->
            toPage Latest.update subMsg subModel NewsLatest NewsLatestMsg

        ( NewsFeed subModel, NewsFeedMsg subMsg ) ->
            toPage Feed.update subMsg subModel NewsFeed NewsFeedMsg

        ( News subModel, NewsMsg subMsg ) ->
            toPage Read.update subMsg subModel News NewsMsg

        ( Edition subModel, EditionMsg subMsg ) ->
            toPage EditionR.update subMsg subModel Edition EditionMsg

        ( Candidates subModel, CandidatesMsg subMsg ) ->
            toPage AppC.update subMsg subModel Candidates CandidatesMsg

        ( Mov subModel, MovMsg subMsg ) ->
            toPage MovD.update subMsg subModel Mov MovMsg

        ( Encounters subModel, EncountersMsg subMsg ) ->
            toPage Enc.update subMsg subModel Encounters EncountersMsg

        ( Article subModel, ArticleMsg subMsg ) ->
            toPage MovA.update subMsg subModel Article ArticleMsg

        ( Participate subModel, ParticipateMsg subMsg ) ->
            toPage Par.update subMsg subModel Participate ParticipateMsg

        ( _, _ ) ->
            model => Cmd.none


listEditions : Cmd Msg
listEditions =
    EditionR.listEditions
        |> RemoteData.sendRequest
        |> Cmd.map ListEditions



---- SUBSCRIPTIONS ----


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ subscriptionsPage model.page
        , Navbar.subscriptions model.navstate NavMsg
        ]


subscriptionsPage : Page -> Sub Msg
subscriptionsPage page =
    case page of
        Participate model ->
            Sub.map ParticipateMsg (Par.subscriptions model)

        _ ->
            Sub.none



---- VIEW ----


view : Model -> Html Msg
view model =
    let
        head =
            header [ class "header" ] [ nav model ]

        page =
            viewPage model.page (Route.locationOrigin model.location) model.flags

        body =
            head :: page
    in
    div [] body


viewPage : Page -> Origin -> Flags -> List (Html Msg)
viewPage page origin flags =
    let
        logo =
            flags.logo

        toWrap toView pageModel toMsg =
            [ div [] (wrap flags.gitIcon (toView pageModel))
                |> Html.map toMsg
            ]
    in
    case page of
        NewsLatest pageModel ->
            toWrap (Latest.view origin logo) pageModel NewsLatestMsg

        NewsFeed pageModel ->
            toWrap (Feed.view origin logo) pageModel NewsFeedMsg

        News pageModel ->
            toWrap (Read.view logo) pageModel NewsMsg

        Edition pageModel ->
            toWrap (EditionR.view logo) pageModel EditionMsg

        Candidates pageModel ->
            toWrap (AppC.view origin logo) pageModel CandidatesMsg

        Mov pageModel ->
            toWrap (MovD.view origin logo) pageModel MovMsg

        Encounters pageModel ->
            toWrap (Enc.view origin logo) pageModel EncountersMsg

        Article pageModel ->
            toWrap (MovA.view logo) pageModel ArticleMsg

        Participate pageModel ->
            toWrap Par.view pageModel ParticipateMsg

        Us ->
            wrap flags.gitIcon us

        NotFound ->
            wrap flags.gitIcon (div [ class "page404" ] [ text "404 Not Found" ])


nav : Model -> Html Msg
nav model =
    let
        origin =
            Route.locationOrigin model.location

        logo =
            img [ width 64, height 64, alt "", src model.flags.logo ] []
    in
    Navbar.config NavMsg
        |> Navbar.withAnimation
        |> Navbar.brand [ Route.href Route.NewsLatest origin, class "rotate-child" ]
            [ logo
            , text "Les Rosalie"
            ]
        |> Navbar.items
            [ Navbar.itemLink [ Route.href Route.NewsFeed origin ] [ text "Actualités" ]
            , Navbar.itemLink [ Route.href Route.Us origin ] [ text "Présentation" ]
            , Navbar.dropdown
                { id = "editionsDropdown"
                , toggle = Navbar.dropdownToggle [] [ text "Nos Éditions" ]
                , items = editionsDropdown model.editions origin
                }
            , Navbar.itemLink [ Route.href Route.Encounters origin ] [ text "Les rencontres" ]
            ]
        |> Navbar.view model.navstate


editionsDropdown : WebData (List Edition) -> Origin -> List (Navbar.DropdownItem msg)
editionsDropdown data origin =
    case data of
        RemoteData.Success editions ->
            List.map (editionItem origin) editions

        _ ->
            []


editionItem : Origin -> Edition -> Navbar.DropdownItem msg
editionItem origin edition =
    Navbar.dropdownItem [ Route.href (Route.Edition edition.id) origin ] [ text edition.name ]



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Navigation.programWithFlags LocationChange
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
