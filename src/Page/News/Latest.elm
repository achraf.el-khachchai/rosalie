module Page.News.Latest exposing (Model, Msg, init, view, update)

import Util exposing ((=>))
import Data.News exposing (News)
import Request.News as NewsR
import View.News as NewsV
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (WebData)
import Route exposing (Origin)


-- MODEL --


type alias Model =
    { feed : WebData (List News)
    }


init : ( Model, Cmd Msg )
init =
    Model RemoteData.Loading => listNews



-- UPDATE --


type Msg
    = ListNews (WebData (List News))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ListNews data ->
            { model | feed = data } => Cmd.none



-- VIEW --


view : Origin -> String -> Model -> Html Msg
view origin logo model =
    div [ class "feed-page" ] [ (NewsV.latest logo model.feed origin) ]



-- INTERNALS --


listNews : Cmd Msg
listNews =
    NewsR.listNews
        |> RemoteData.sendRequest
        |> Cmd.map ListNews
