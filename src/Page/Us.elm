module Page.Us exposing (us)

import Html exposing (..)
import Html.Attributes exposing (..)
import Markdown exposing (toHtmlWith, defaultOptions, Options)


us : Html msg
us =
    div [ class "page-us content" ]
        [ h1 [] [ text "Présentation" ]
        , div [ class "readable-content" ]
          [ h2 [] [ text "Les Rosalie" ]
          , toHtmlWith options [] body
          ]
        ]

options : Options
options =
    { defaultOptions | sanitize = True}

body : String
body =
  """
Les « Rosalie du cinéma » sont un ensemble de prix récompensant les productions cinématographiques françaises les plus remarquables de l’année écoulée. Elles ont été fondées en 2017 en réaction à une programmation des César jugée décevante. Elles mènent depuis lors un chemin alternatif à celui des César et récompensent ce qu’elles jugent être les meilleurs films français. Dans un hommage évident au film de Claude Sautet, César et Rosalie, elles offrent une récompense artistique alternative portant un prénom féminin, là où les noms des prix sont d’ordinaire masculins.

  Les Rosalie promeuvent un cinéma français neuf et original, à même de plaire au plus grand nombre, sans jamais sacrifier la dimension artistique des œuvres. Le processus de sélection des films correspond à une idée du cinéma selon laquelle il n’est pas l’apanage des seuls spécialistes. Le mode de scrutin diffère donc des prix classiques et, plutôt que de ne se réserver qu’à un jury de professionnels, il est ouvert à toute personne convaincue que le cinéma français est pourvu de qualités artistiques trop ignorées par les prix traditionnels ou par le système médiatique. Le plus souvent, il en résulte un choix de films aux productions plus modestes et aux publics plus réduits que d’ordinaire.
  
  Les Rosalie ne considèrent pas que le jeu d’actrice soit essentiellement différent du jeu d’acteur, pas plus qu’elles ne reconnaissent une prévalence des premiers rôles sur les seconds. C’est pourquoi elles ne proposent que deux prix d’interprétation, non-genrés et non-hiérarchiques : « Meilleure Interprétation » et « Meilleur Espoir d’Interprétation ».
  
  Les Rosalie ne font pas non plus de différence fondamentale entre les différents régimes de films que sont le documentaire, l’animation ou la fiction. Tout film et toute performance artistique est donc éligible à n’importe quel prix, y compris « Meilleur Film » et « Meilleure Réalisation ».
  
  Les Rosalie ne cessent de réfléchir à ce que signifie : « récompenser un film ». Plutôt que d’être offertes à l’occasion de cérémonies auxquelles viennent les nommés, ce sont les Rosalie qui vont aux artistes qu’elles récompensent. Elles sont ainsi remises en mains propres à leurs lauréats, dans la situation la moins formelle possible. Ces rencontres sont animées de discussions qui portent sur le parcours et le travail de l’artiste primé. Chaque rencontre est ensuite retranscrite et diffusée sur le présent site internet, dans la conviction que le geste de médiatiser les débats, les idées et les formes qui innervent le nouveau cinéma français ne peut qu’améliorer la perception que peut avoir le grand public de ce cinéma.
"""
