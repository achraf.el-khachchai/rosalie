module Page.Edition.Encounters exposing (Model, Msg, init, update, view)

import Util exposing ((=>))
import View.Edition exposing (grid)
import Data.Edition exposing (Encounter)
import Request.Edition as EdiR
import Html exposing (..)
import Html.Attributes exposing (class, src)
import RemoteData exposing (WebData)
import Route exposing (Origin)


-- MODEL --


type alias Model =
    { encounters : WebData (List Encounter)
    }


init : ( Model, Cmd Msg )
init =
    Model RemoteData.Loading => listEncounters



-- UPDATE --


type Msg
    = ListEncounters (WebData (List Encounter))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ListEncounters data ->
            { model | encounters = data } => Cmd.none



-- VIEW --


view : Origin -> String -> Model -> Html Msg
view origin logo model =
    div [ class "encounters-page" ]
        [ (grid logo model.encounters origin) ]



-- INTERNALS --


listEncounters : Cmd Msg
listEncounters =
    EdiR.listEncounters
        |> RemoteData.sendRequest
        |> Cmd.map ListEncounters
