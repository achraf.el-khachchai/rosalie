module Page.Edition.Retrieve exposing (Model, Msg, init, update, view)

import Data.Award exposing (Award)
import Data.Edition
    exposing
        ( Edition
        , EditionId
        , Interaction(..)
        , editionInteract
        , interactStr
        )
import Date exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import RemoteData exposing (WebData)
import Request.Edition as EditionR
import Route exposing (Origin)
import Task exposing (..)
import Util exposing ((=>))
import View.Edition as EditionV
import View.Util exposing (dateStr)


-- MODEL --


type alias Model =
    { edition : WebData Edition
    , awards : WebData (List Award)
    , interaction : Maybe Interaction
    , origin : Origin
    }


init : EditionId -> Origin -> ( Model, Cmd Msg )
init eid origin =
    Model RemoteData.Loading RemoteData.Loading Nothing origin => retrieveEdition eid



-- UPDATE --


type Msg
    = RetrieveEditions (WebData Edition)
    | ListAwards (WebData (List Award))
    | InteractNow Edition Date
    | Interact


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RetrieveEditions data ->
            let
                cmd =
                    case data of
                        RemoteData.Success edition ->
                            Cmd.batch
                                [ listAwards edition.id
                                , interactNow edition
                                ]

                        _ ->
                            Cmd.none
            in
                { model | edition = data } => cmd

        ListAwards data ->
            { model | awards = data } => Cmd.none

        InteractNow edition now ->
            { model | interaction = Just (editionInteract edition now) } => Cmd.none

        Interact ->
            let
                cmd =
                    case model.interaction of
                        Just interaction ->
                            Route.interactDest interaction model.origin

                        Nothing ->
                            Cmd.none
            in
                model => cmd



-- VIEW --


view : String -> Model -> Html Msg
view logo model =
    let
        ( awardsView, interactView ) =
            case model.interaction of
                Just interaction ->
                    ( EditionV.listAwards logo model.awards interaction model.origin
                    , interactEdition interaction model.origin logo
                    )

                Nothing ->
                    ( div [] []
                    , div [] []
                    )

        editionView =
            EditionV.detail logo model.edition awardsView interactView
    in
        div [ class "edition-page" ] [ editionView ]



-- INTERNALS --


retrieveEdition : EditionId -> Cmd Msg
retrieveEdition eid =
    EditionR.retrieveEdition eid
        |> RemoteData.sendRequest
        |> Cmd.map RetrieveEditions


listAwards : EditionId -> Cmd Msg
listAwards eid =
    EditionR.listEditionAwards eid
        |> RemoteData.sendRequest
        |> Cmd.map ListAwards


interactNow : Edition -> Cmd Msg
interactNow edition =
    Task.perform (InteractNow edition) Date.now


interactEdition : Interaction -> Origin -> String -> Html Msg
interactEdition interaction origin logo =
    let
        view =
            case interaction of
                Over eid ->
                    []

                Starting date ->
                    [ img [ class "align-center width-half logo-rotate", src logo ] []
                    , p [ class "err font-weight-bold text-center" ]
                        [ text ("Cette édition débutera le " ++ (dateStr date)) ]
                    ]

                Ask eid ->
                    [ button [ class "btn btn-info btn-middle", onClick Interact ]
                        [ text (interactStr interaction) ]
                    , p [ class "text-right" ]
                        [ a [ Route.href (Route.Candidates eid) origin ]
                            [ text "Consulter les candidats" ]
                        ]
                    ]

                _ ->
                    -- TODO vote MSG
                    []
    in
        div [ class "edition-interact" ] view
