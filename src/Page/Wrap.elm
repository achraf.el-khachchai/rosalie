module Page.Wrap exposing (wrap)

import Html exposing (..)
import Html.Attributes exposing (..)


wrap : String -> Html msg -> List (Html msg)
wrap icon content =
    [ hr [] []
    , div [ class "page" ] [ content ]
    , viewFooter icon
    ]


viewFooter : String -> Html msg
viewFooter icon =
    footer [ class "footer" ]
      [ span [ class "footer-title" ] [ text "Web Devs :" ]
      , ul []
      [ li []
      [ text "Justin Rerolle"
      , a [ href "https://github.com/justinrlle" ]
      [ img [ width 20, height 20, src icon, class "dev-icon" ] [] ]
      ]
      , li [ href "https://github.com/Lyusan" ]
      [ text "Léo Champion"
      , a [ href "https://github.com/Lyusan" ]
      [ img [ width 20, height 20, src icon, class "dev-icon" ] [] ]
      ]
      , li []
      [ text "Achraf El Khachchai"
      , a [ href "https://github.com/iLascap" ]
      [ img [ width 20, height 20, src icon, class "dev-icon" ] [] ]
      ]
      ]
      ]



{--
viewHeader : Html msg
viewHeader =
    let
        logo =
            img [ width 64, height 64, alt "", src "https://upload.wikimedia.org/wikipedia/commons/9/9b/Heraldisch_Lippische_Rose.svg" ] []

        brand =
            a [ class "navbar-brand", Route.href Route.NewsLatest ]
                [ logo
                , text "Les Rosalie"
                ]

        naview =
            brand :: [] --navRefs
    in
        header [ class "header" ]
            [ nav [ class "navbar " ] naview
            , hr [ class "headerhr" ] []
            ]


navRefs : List (Html msg)
navRefs =
    List.map hrefLink navLinks



navLinks : List Link
navLinks =
    [ Link Route.NewsFeed "Actualités"
    , Link Route.Us "Présentation"
    , Link Route.Editions "Nos Éditions"
    , Link Route.Encounters "Les rencontres"
    ]


type alias Link =
    { dest : Route, label : String }


hrefLink : Link -> Html msg
hrefLink link =
    a [ class "nav-link", Route.href link.dest ]
        [ text link.label ]
--}
