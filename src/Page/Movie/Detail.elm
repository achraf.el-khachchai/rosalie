module Page.Movie.Detail exposing (Model, Msg, init, update, view)

import Data.Article exposing (Article)
import Data.Movie exposing (Movie)
import Html exposing (..)
import Html.Attributes exposing (class)
import RemoteData exposing (WebData)
import Request.Movie as MovR
import Util exposing ((=>))
import View.Movie exposing (detail, articleList)
import Route exposing (Origin)


-- MODEL --


type alias Model =
    { movie : WebData Movie
    , articles : WebData (List Article)
    }


init : Int -> ( Model, Cmd Msg )
init i =
    Model RemoteData.Loading RemoteData.Loading
        => retrieveMovie i



-- UPDATE --


type Msg
    = RetrieveMovie (WebData Movie)
    | ListArticles (WebData (List Article))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RetrieveMovie data ->
            let
                cmd =
                    case data of
                        RemoteData.Success movie ->
                            listArticles movie.id

                        _ ->
                            Cmd.none
            in
                { model | movie = data } => cmd

        ListArticles data ->
            { model | articles = data } => Cmd.none



-- VIEW --


view : Origin -> String -> Model -> Html Msg
view origin logo model =
    let
        articles =
            [ articleList logo model.articles origin ]
    in
        div [ class "movie-page" ] [ detail logo model.movie articles ]



-- INTERNALS --


retrieveMovie : Int -> Cmd Msg
retrieveMovie mid =
    MovR.retrieveMovie mid
        |> RemoteData.sendRequest
        |> Cmd.map RetrieveMovie


listArticles : Int -> Cmd Msg
listArticles mid =
    MovR.listArticles mid
        |> RemoteData.sendRequest
        |> Cmd.map ListArticles
