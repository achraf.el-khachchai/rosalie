module Page.Application.Candidates exposing (Model, Msg, init, update, view)

import Data.Award exposing (Award, AwardId)
import Data.Edition exposing (EditionId, Interaction(..))
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (WebData)
import Request.Edition as EditionR
import Route exposing (Origin)
import Util exposing ((=>))
import View.Edition as EditionV


-- MODEL --


type alias Model =
    { awards : WebData (List Award)
    , eid : EditionId
    }


init : EditionId -> ( Model, Cmd Msg )
init eid =
    Model RemoteData.Loading eid => listAwards eid



-- UPDATE --


type Msg
    = ListAwards (WebData (List Award))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ListAwards data ->
            { model | awards = data } => Cmd.none



-- VIEW --


view : Origin -> String -> Model -> Html Msg
view origin logo model =
    div [ class "page-candidates content" ]
        [ h1 [] [ text "Candidats" ]
        , EditionV.listAwards logo model.awards (Nominate model.eid) origin
        , a [ class "text-right", Route.href (Route.Edition model.eid) origin ]
            [ text "Retourner à l'édition" ]
        ]



-- INTERNALS --


listAwards : EditionId -> Cmd Msg
listAwards eid =
    EditionR.listEditionAwards eid
        |> RemoteData.sendRequest
        |> Cmd.map ListAwards
