module Page.Participate exposing (Model, Msg, init, subscriptions, update, view)

import Bootstrap.Button as Button
import Data.Application exposing (App, AppId, appname, awappname)
import Data.Award exposing (Award, awardWinners, awardsWinnersIds)
import Data.Edition exposing (Edition, EditionId)
import Data.Participate
    exposing
        ( Error
        , Field(..)
        , Question
        , Validity(..)
        , clean
        , newAsk
        , questionAsk
        , questionCheck
        , questionSelect
        )
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Ports exposing (render, setToken)
import RemoteData exposing (WebData)
import Request.Edition as EditionR
import Util exposing ((=>))
import Validate exposing (Validator, ifFalse, ifNothing, isBlank, isValidEmail, validate)


-- MODEL --


type alias Model =
    { mail : String
    , questions : Dict String Question
    , token : Maybe String
    , questionsCounter : Int
    , checkMail : Validity
    , checkToken : Validity
    , eid : EditionId
    , winners : List ( String, App )
    , winnersIds : Dict String AppId
    }


container : String
container =
    "reContainer"


init : EditionId -> ( Model, Cmd Msg )
init eid =
    Model "" Dict.empty Nothing 0 None None eid [] Dict.empty => getAwards eid


validator : Validator Error Model
validator =
    Validate.all
        [ ifFalse (\model -> isBlank model.mail || isValidEmail model.mail)
            (Error MailField "Email incorrect, vérifiez la saisie.")
        , ifFalse (\model -> List.isEmpty <| List.filter String.isEmpty <| List.map .ask <| Dict.values model.questions)
            (Error AskField "Ne soyez pas timide, posez votre question.")
        , ifNothing .token
            (Error Recaptcha "Aidez nous à vaincre l'armée de robots du web, un reCaptcha à la fois.")
        ]



-- SUBSCRIPTIONS --


subscriptions : Model -> Sub Msg
subscriptions model =
    setToken Token



-- UPDATE --


type Msg
    = Mail String
    | NewAsk
    | DelAsk String
    | Ask String String
    | Select String String
    | Token String
    | Validate
    | CreateQuestions (WebData Bool)
    | GetAwards (WebData (List Award))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Mail mail ->
            { model | mail = mail } => Cmd.none

        NewAsk ->
            let
                ( key, val ) =
                    newAsk model.questionsCounter model.winners model.winnersIds

                questions =
                    Dict.insert key val model.questions

                cmd =
                    if model.questionsCounter == 0 then
                        render container
                    else
                        Cmd.none
            in
                { model | questions = questions, questionsCounter = model.questionsCounter + 1 } => cmd

        DelAsk key ->
            { model | questions = Dict.remove key model.questions } => Cmd.none

        Ask key ask ->
            { model | questions = Dict.update key (questionAsk ask) model.questions } => Cmd.none

        Select key str ->
            let
                choice =
                    Dict.get str model.winnersIds

                questions =
                    Dict.update key (questionSelect choice) model.questions
            in
                { model | questions = questions } => Cmd.none

        Token token ->
            { model | token = Just token } => Cmd.none

        Validate ->
            let
                errs =
                    validate validator model

                mail =
                    clean errs MailField

                token =
                    clean errs Recaptcha

                questions =
                    Dict.map questionCheck model.questions

                cmd =
                    case errs of
                        [] ->
                            createQuestions model

                        errs ->
                            Cmd.none
            in
                { model | checkMail = mail, checkToken = token, questions = questions } => cmd

        CreateQuestions data ->
            model => Cmd.none

        GetAwards data ->
            case data of
                RemoteData.Success awards ->
                    let
                        winners =
                            List.concat (List.map awardWinners awards)

                        winnersIds =
                            Dict.fromList (awardsWinnersIds awards)
                    in
                        update NewAsk { model | winners = winners, winnersIds = winnersIds }

                _ ->
                    model => getAwards model.eid


createQuestions : Model -> Cmd Msg
createQuestions model =
    EditionR.createQuestions model
        |> RemoteData.sendRequest
        |> Cmd.map CreateQuestions


getAwards : EditionId -> Cmd Msg
getAwards eid =
    EditionR.listEditionAwards eid
        |> RemoteData.sendRequest
        |> Cmd.map GetAwards



-- VIEW --


view : Model -> Html Msg
view model =
    let
        options =
            List.map (\app -> option [] [ text (awappname app) ]) model.winners

        asksView =
            List.map (ask options) (Dict.toList model.questions)

        mailHelp =
            "Facultatif. Nous ne transmettrons jamais votre email à qui que ce soit."

        mailView =
            [ forminput "email" "Email" "mail" model.mail mailHelp Mail model.checkMail
            , button [ class "btn btn-success btn-middle", onClick NewAsk ]
                [ text "Ajouter une Question" ]
            ]

        sendView =
            [ reView model.checkToken
            , p []
                [ span [ class "color-red font-weight-bold" ] [ text "* " ]
                , span [ class "font-italic color-gray" ] [ text "champs obligatoires" ]
                ]
            , button [ class "btn btn-primary btn-middle", onClick Validate ]
                [ text "Envoyer" ]
            ]

        views =
            mailView ++ asksView
    in
        div [ class "ask-page content" ]
            [ h1 [] [ text "Vos questions ?" ]
            , div [] views
            , div [] sendView
            ]


forminput : String -> String -> String -> String -> String -> (String -> Msg) -> Validity -> Html Msg
forminput inputType inputLabel inputId value inputHelp msg validity =
    let
        ( grpClass, ctrlClass, feedback ) =
            validHtml validity

        help =
            [ small [ class "form-text text-muted" ] [ text inputHelp ]
            ]

        control =
            [ label [ class "form-control-label", for inputId ]
                [ text inputLabel ]
            , input [ type_ inputType, class "form-control", ctrlClass, id inputId, onInput msg ]
                [ text value ]
            ]

        content =
            control ++ feedback ++ help
    in
        div [ class "form-group", grpClass ] content


ask : List (Html Msg) -> ( String, Question ) -> Html Msg
ask winners ( str, question ) =
    let
        questionView =
            txtColInput "Question" ("ask" ++ str) question.ask (Ask str) question.check

        selectView =
            [ div [ class "form-group row" ]
                [ label [ class "col-form-label col-sm-2", for (str ++ "_sel") ] [ text "Pour" ]
                , div [ class "col-sm-8" ]
                    [ select [ class "form-control", id (str ++ "_sel"), onInput (Select str) ] winners ]
                , div [ class "col-sm-2" ]
                    [ Button.button [ Button.danger, Button.onClick (DelAsk str) ] [ text "Supprimer" ] ]
                ]
            ]
    in
        div [ class "question-row" ] (questionView ++ selectView)


reView : Validity -> Html msg
reView check =
    let
        ( grpClass, hidenClass, help ) =
            validHtml check
    in
        div [ class "form-group", grpClass ]
            [ div [ id container ] []
            , div [ id "reForm" ] help
            ]


validHtml : Validity -> ( Attribute msg, Attribute msg, List (Html msg) )
validHtml valid =
    case valid of
        None ->
            ( class "", class "", [] )

        Right ->
            ( class "has-success"
            , class "form-control-success"
            , [ div [ class "form-control-feedback" ] [ text "Looks good !" ] ]
            )

        Wrong err ->
            ( class "has-danger"
            , class "form-control-danger"
            , [ div [ class "form-control-feedback" ] [ text err ] ]
            )


txtColInput : String -> String -> String -> (String -> Msg) -> Validity -> List (Html Msg)
txtColInput txtlabel txtid txt msg valid =
    let
        ( grpClass, inputClass, help ) =
            validHtml valid

        inputView =
            [ label [ class "col-form-label col-sm-2", for txtid ]
                [ text txtlabel
                , span [ class "color-red font-weight-bold" ] [ text " *" ]
                ]
            , div [ class "col-sm-8" ]
                ([ input [ inputClass, class "form-control", id txtid, type_ "text", onInput msg ] [ text txt ]
                 ]
                    ++ help
                )
            ]

        view =
            inputView
    in
        [ div [ class "form-group row", grpClass ]
            view
        ]
