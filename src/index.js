import './main.css';
import logoPath from '../static/logo.png'
import gitIconPath from '../static/git.png'
import { Main } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

window.onloadCallback = () => {
  const flags = {
    'logo': logoPath,
    'gitIcon': gitIconPath
  }
  const app = Main.embed(document.getElementById('root'), flags);
  let captcha;

  app.ports.render.subscribe((containerId) => {
    window.requestAnimationFrame(() => {
      const container = document.getElementById(containerId);
      const params = {
        'sitekey': '6Ler_EkUAAAAAL9p7upnmJmj548bu_FY_o5avhgj',
        'callback': app.ports.setToken.send
      };
      captcha = grecaptcha.render(container, params);
    })
  });
};

registerServiceWorker();
