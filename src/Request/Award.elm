module Request.Award
    exposing
        ( listAwards
        , listCandidates
        , retrieveAward
        , retrieveWinner
        )

import Data.Application as App
import Data.Award exposing (Award, AwardId, aidStr, awardsDecoder, decoder)
import Http
import Request.Api exposing (apiUrl)


listAwards : Http.Request (List Award)
listAwards =
    Http.get awardsUrl awardsDecoder


retrieveAward : AwardId -> Http.Request Award
retrieveAward aid =
    Http.get (awardUrl aid) decoder


retrieveWinner : AwardId -> Http.Request App.App
retrieveWinner aid =
    Http.get (winnerUrl aid) App.decoder


listCandidates : AwardId -> Http.Request (List App.App)
listCandidates aid =
    Http.get (candidatesUrl aid) App.appsDecoder



-- INTERNALS --


awardsUrl : String
awardsUrl =
    apiUrl ++ "/awards"


awardUrl : AwardId -> String
awardUrl aid =
    awardsUrl ++ "/" ++ aidStr aid


winnerUrl : AwardId -> String
winnerUrl aid =
    awardUrl aid ++ "/winner"


candidatesUrl : AwardId -> String
candidatesUrl aid =
    awardUrl aid ++ "/applications?nominees=false"
