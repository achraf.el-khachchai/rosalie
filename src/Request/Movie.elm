module Request.Movie
    exposing
        ( retrieveMovie
        , listArticles
        , retrieveArticle
        )

import Data.Article
    exposing
        ( Article
        , articleDecoder
        , articlesDecoder
        )
import Data.Movie exposing (Movie, movieDecoder)
import Http
import Request.Api exposing (apiUrl)


retrieveMovie : Int -> Http.Request Movie
retrieveMovie mid =
    Http.get (movieUrl mid) movieDecoder


listArticles : Int -> Http.Request (List Article)
listArticles mid =
    Http.get (articlesUrl mid) articlesDecoder


retrieveArticle : Int -> Http.Request Article
retrieveArticle aid =
    Http.get (articleUrl aid) articleDecoder



-- INTERNALS --


moviesUrl : String
moviesUrl =
    apiUrl ++ "/movies"


movieUrl : Int -> String
movieUrl mid =
    moviesUrl ++ "/" ++ toString mid


articlesUrl : Int -> String
articlesUrl mid =
    movieUrl mid ++ "/articles"


articleUrl : Int -> String
articleUrl aid =
    apiUrl ++ "/articles/" ++ toString aid
