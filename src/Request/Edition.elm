module Request.Edition
    exposing
        ( createQuestions
        , listEditionAwards
        , listEditions
        , listEncounters
        , retrieveEdition
        )

import Data.Award exposing (Award, AwardId, aidStr, awardsDecoder)
import Data.Edition
    exposing
        ( Edition
        , EditionId
        , Encounter
        , decoder
        , editionsDecoder
        , eidStr
        , encountersDecoder
        )
import Data.Participate exposing (Form, decodeResponse, encode)
import Http
import Request.Api exposing (apiUrl)


listEditions : Http.Request (List Edition)
listEditions =
    Http.get editionsUrl editionsDecoder


retrieveEdition : EditionId -> Http.Request Edition
retrieveEdition eid =
    Http.get (editionUrl eid) decoder


listEditionAwards : EditionId -> Http.Request (List Award)
listEditionAwards eid =
    Http.get (editionAwardsUrl eid) awardsDecoder


listEncounters : Http.Request (List Encounter)
listEncounters =
    Http.get encountersUrl encountersDecoder


createQuestions : Form r -> Http.Request Bool
createQuestions form =
    let
        url =
            questionsUrl

        body =
            Http.jsonBody (encode form)
    in
    Http.post url body decodeResponse



-- INTERNALS --


editionsUrl : String
editionsUrl =
    apiUrl ++ "/editions"


editionUrl : EditionId -> String
editionUrl eid =
    editionsUrl ++ "/" ++ eidStr eid


editionAwardsUrl : EditionId -> String
editionAwardsUrl eid =
    editionUrl eid ++ "/awards"


encountersUrl : String
encountersUrl =
    apiUrl ++ "/encounters"


questionsUrl : String
questionsUrl =
    apiUrl ++ "/questions"
