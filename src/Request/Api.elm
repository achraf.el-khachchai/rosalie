module Request.Api exposing (apiUrl)


apiUrl : String
apiUrl =
    "/api/v1"
