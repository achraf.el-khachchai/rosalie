port module Ports exposing (render, setToken)

-- OUT PORTS --


port render : String -> Cmd msg



-- IN PORTS --


port setToken : (String -> msg) -> Sub msg
