module View.Util exposing (dataview, dateStr, buildingView)

import Date exposing (Date, year, month, day)
import Date.Format exposing (format)
import Html exposing (..)
import Html.Attributes exposing (class, src)
import Http exposing (Error, Error(..))
import RemoteData exposing (WebData)


dateStr : Date -> String
dateStr date =
    format "%d/%m/%y" date


dataview : String -> WebData a -> (a -> List (Html msg)) -> List (Html msg)
dataview logo data success =
    case data of
        RemoteData.NotAsked ->
            [ img [ class "align-center width-half logo-static", src logo ] []
            ]

        RemoteData.Loading ->
            [ img [ class "align-center width-half logo-rotate", src logo ] []
            ]

        RemoteData.Failure err ->
            [ img [ class "align-center width-half logo-static", src logo ] []
            , p [ class "err font-weight-bold color-red text-center" ]
                [ text "404 - Rosalie Not Found" ]
            ]

        RemoteData.Success a ->
            success a


buildingView : String -> List (Html msg)
buildingView logo =
    [ img [ class "align-center width-half logo-rotate", src logo ] []
    , p [ class "err font-weight-bold text-center" ]
        [ text "Page en construction ..." ]
    , p [ class "err font-weight-bold text-center" ]
        [ text "Revenez vite nous voir !" ]
    ]



-- INTERNALS --


printError : Error -> Html msg
printError err =
    case err of
        BadUrl url ->
            text ("Bad url" ++ url)

        Timeout ->
            text "Request timeout"

        NetworkError ->
            text "No network: are you sure you're connected to the internet?"

        BadStatus response ->
            text ("Bad status: got " ++ (toString response.status.code))

        BadPayload msg response ->
            text ("Failed to parse response: " ++ msg)
