module View.Edition exposing (detail, grid, list, listAwards)

import Data.Award exposing (Award)
import Data.Edition
    exposing
        ( Edition
        , Encounter
        , Interaction(..)
        , interactStr
        , interactTitle
        )
import Data.Movie exposing (Movie)
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (WebData)
import Route exposing (Origin)
import View.Application as AppV
import View.Util exposing (dataview)


detail : String -> WebData Edition -> Html msg -> Html msg -> Html msg
detail logo data awards interact =
    let
        view =
            editionDetail awards interact
    in
    div [ class "edition-detail content" ] (dataview logo data view)


list : String -> WebData (List Edition) -> Origin -> Html msg
list logo data origin =
    div [ class "edition-list" ] (dataview logo data (editionList origin))


grid : String -> WebData (List Encounter) -> Origin -> Html msg
grid logo data origin =
    let
        head =
            [ h1 [] [ text "Les Rencontres" ]
            , rencontres
            ]

        encounters =
            dataview logo data (encounterGrid origin)

        content =
            head ++ encounters
    in
    div [ class "encounter-grid content" ] content


listAwards : String -> WebData (List Award) -> Interaction -> Origin -> Html msg
listAwards logo data interaction origin =
    div [ class "edition-awards" ]
        (dataview logo data (awardList interaction origin logo))



-- INTERNALS --


editionDetail : Html msg -> Html msg -> Edition -> List (Html msg)
editionDetail awards interact edition =
    [ h1 [] [ text edition.name ]
    , awards
    , interact
    ]


editionList : Origin -> List Edition -> List (Html msg)
editionList origin editions =
    List.map (editionRow origin) editions


editionRow : Origin -> Edition -> Html msg
editionRow origin edition =
    div [ class "edition-row" ]
        [ a [ Route.href (Route.Edition edition.id) origin ]
            [ text edition.name ]
        ]


encounterGrid : Origin -> List Encounter -> List (Html msg)
encounterGrid origin encounters =
    List.map (encounterElem origin) encounters


encounterElem : Origin -> Encounter -> Html msg
encounterElem origin encounter =
    let
        head =
            h2 [] [ text encounter.edition.name ]

        movies =
            List.map (movieElem origin) encounter.movies

        content =
            head :: movies
    in
    div [ class "encounter" ] content


movieElem : Origin -> Movie -> Html msg
movieElem origin movie =
    a [ Route.href (Route.Movie movie.id) origin ]
        [ img [ class "movie-elem", src movie.img, alt movie.title ] [] ]


awardList : Interaction -> Origin -> String -> List Award -> List (Html msg)
awardList interaction origin logo awards =
    List.map (awardRow interaction origin logo) awards


awardRow : Interaction -> Origin -> String -> Award -> Html msg
awardRow interaction origin logo award =
    let
        applications =
            case interaction of
                Starting date ->
                    []

                Nominate eid ->
                    AppV.list origin award.candidates

                Reward eid ->
                    AppV.list origin award.nominees

                _ ->
                    let
                        wins =
                            award.winners

                        nominees =
                            List.filter (\app -> not (List.member app wins)) award.nominees
                    in
                    AppV.listWins origin logo wins
                        ++ AppV.list origin nominees
    in
    div [ class "award-row" ]
        [ h2 [] [ text award.name ]
        , div [ class "award-applications" ] applications
        ]


rencontres : Html msg
rencontres =
    p [ class "readable-content" ] [ text "Vous savez, moi je ne crois pas qu’il y ait de bonne ou de mauvaise situation. Moi, si je devais résumer ma vie aujourd’hui avec vous, je dirais que c’est d’abord des rencontres. Des gens qui m’ont tendu la main, peut-être à un moment où je ne pouvais pas, où j’étais seul chez moi. Et c’est assez curieux de se dire que les hasards, les rencontres forgent une destinée... Parce que quand on a le goût de la chose, quand on a le goût de la chose bien faite, le beau geste, parfois on ne trouve pas l’interlocuteur en face je dirais, le miroir qui vous aide à avancer. Alors ça n’est pas mon cas, comme je disais là, puisque moi au contraire, j’ai pu : et je dis merci à la vie, je lui dis merci, je chante la vie, je danse la vie... je ne suis qu’amour ! Et finalement, quand beaucoup de gens aujourd’hui me disent « Mais comment fais-tu pour avoir cette humanité ? », et bien je leur réponds très simplement, je leur dis que c’est ce goût de l’amour ce goût donc qui m’a poussé aujourd’hui à entreprendre une construction mécanique, mais demain qui sait ? Peut-être simplement à me mettre au service de la communauté, à faire le don, le don de soi... " ]
