module View.News exposing (detail, latest, list)

import Data.News exposing (News, orderNews)
import Html exposing (..)
import Html.Attributes exposing (..)
import Markdown
import RemoteData exposing (WebData)
import Route exposing (Origin)
import View.Util exposing (dataview, dateStr)


detail : String -> WebData News -> Html msg
detail logo data =
    div [ class "news-detail" ] (dataview logo data newsDetail)


latest : String -> WebData (List News) -> Origin -> Html msg
latest logo data origin =
    div [ class "news-latest content" ]
        (h1 [] [ text "Les Rosalie du cinéma" ]
            :: dataview logo data (newsLatest origin)
        )


list : String -> WebData (List News) -> Origin -> Html msg
list logo data origin =
    div [ class "news-list content" ]
        (h1 [] [ text "Les Rosalie du cinéma" ]
            :: dataview logo data (newsList origin)
        )



-- INTERNALS --


newsDetail : News -> List (Html msg)
newsDetail news =
    [ img [ class "news-banner", src news.img, alt "" ] []
    , div [ class "news-body content" ]
        [ p [ class "news-author text-right font-italic" ] [ text ("Par " ++ news.author ++ " le " ++ dateStr news.pub) ]
        , h1 [ class "news-title" ] [ text news.title ]
        , p [ class "news-content readable-content" ] [ Markdown.toHtml [] news.content ]
        ]
    ]


newsList : Origin -> List News -> List (Html msg)
newsList origin feed =
    List.map (newsCard origin) (orderNews feed)


newsLatest : Origin -> List News -> List (Html msg)
newsLatest origin feed =
    let
        ordered =
            orderNews feed
    in
    case ordered of
        [] ->
            []

        latest :: others ->
            [ div [ class "mainnews" ] [ newsCard origin latest ]
            , div [ class "news-row" ] (List.map (newsRow origin) (List.take 3 others))
            ]


newsRow : Origin -> News -> Html msg
newsRow origin news =
    div [ class "subnews" ]
        [ newsCard origin news ]


newsCard : Origin -> News -> Html msg
newsCard origin news =
    div [ class "news-card" ]
        [ a [ class "card-link", Route.href (Route.News news.id) origin ]
            [ img [ class "card-img-top", src news.img, alt "" ] []
            , div [ class "body" ]
                [ h5 [ class "card-title" ] [ text news.title ]
                , p [ class "card-text" ] [ text news.summary ]
                ]
            ]
        ]
