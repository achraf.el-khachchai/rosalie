module View.Application exposing (detail, list, listWins, winnerRow)

import Data.Application exposing (App, appname, fullname)
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (WebData)
import Route exposing (Origin)
import View.Util exposing (dataview)


detail : String -> WebData App -> List (Html msg) -> Html msg
detail logo data appArticles =
    div [ class "application-detail content" ]
        (dataview logo data appDetail ++ appArticles)


list : Origin -> List App -> List (Html msg)
list origin apps =
    apps
        |> List.map (appRow False origin)
        |> List.map (\row -> div [ class "approw" ] [ row ])


listWins : Origin -> String -> List App -> List (Html msg)
listWins origin logo wins =
    wins
        |> List.map (winnerRow origin logo)
        |> List.concat


winnerRow : Origin -> String -> App -> List (Html msg)
winnerRow origin logo winner =
    [ div [ class "winrow rotate-child" ]
        [ img [ class "reward-logo", src logo, width 60, height 60, alt "" ] []
        , appRow True origin winner
        ]
    ]



-- INTERNALS --


appDetail : App -> List (Html msg)
appDetail app =
    [ h1 []
        [ text app.movie.title ]
    , h2 [] [ text app.movie.title ]
    , p [] [ text app.movie.desc ]
    ]


appRow : Bool -> Origin -> App -> Html msg
appRow bold origin app =
    let
        fontweight =
            if bold then
                "font-weight-bold"
            else
                "font-weight-normal"
    in
    p [ class fontweight ]
        [ text (fullname app.person ++ " pour ")
        , a [ Route.href (Route.Movie app.movie.id) origin ]
            [ text app.movie.title ]
        ]
