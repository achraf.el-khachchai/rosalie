module View.Movie
    exposing
        ( detail
        , articleDetail
        , articleList
        )

import Data.Article exposing (Article)
import Data.Movie exposing (Movie)
import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData exposing (WebData)
import Route exposing (Origin)
import View.Util exposing (dataview)


detail : String -> WebData Movie -> List (Html msg) -> Html msg
detail logo data movArticles =
    div [ class "movlication-detail content" ]
        ((dataview logo data movDetail) ++ movArticles)


articleList : String -> WebData (List Article) -> Origin -> Html msg
articleList logo data origin =
    div [ class "article-list" ] (dataview logo data (listArticle origin))


articleDetail : String -> WebData Article -> Html msg
articleDetail logo data =
    div [ class "article-detail content" ] (dataview logo data detailArticle)



-- INTERNALS --


movDetail : Movie -> List (Html msg)
movDetail movie =
    [ h1 []
        [ text movie.title ]
    , img [ class "movie-img", src movie.img, alt movie.title ] []
    , h2 [] [ text "Synopsis" ]
    , p [] [ text movie.desc ]
    ]


listArticle : Origin -> List Article -> List (Html msg)
listArticle origin articles =
    case articles of
        [] ->
            []

        articles ->
            h2 [] [ text "Rencontres" ]
                :: List.map (articleRow origin) articles


detailArticle : Article -> List (Html msg)
detailArticle article =
    [ h1 [] [ text article.title ]
    , p [ class "readable-content" ] [ text article.content ]
    ]


articleRow : Origin -> Article -> Html msg
articleRow origin article =
    div [ class "article-row" ]
        [ a [ Route.href (Route.Article article.id) origin ]
            [ text article.title ]
        ]
