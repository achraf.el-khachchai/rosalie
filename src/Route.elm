module Route
    exposing
        ( Route(..)
        , Origin(..)
        , locationOrigin
        , href
        , interactDest
        , modifyUrl
        , parseLocation
        )

import Data.Edition exposing (EditionId, Interaction(..), eidInt, eidParser, eidStr)
import Data.News exposing (NewsId, newsidParser, nidStr)
import Html exposing (Attribute)
import Html.Attributes as Attr
import Navigation exposing (Location)
import UrlParser exposing ((</>), Parser, map, oneOf, parseHash, s, top)


type Route
    = NewsLatest
    | NewsFeed
    | News NewsId
    | Us
    | Edition EditionId
    | Candidates EditionId
    | Encounters
    | Movie Int
    | Article Int
    | Participate EditionId
    | NotFound


parser : Parser (Route -> a) a
parser =
    oneOf
        [ map NewsLatest top
        , map NewsLatest (s "")
        , map NewsFeed (s "actualites")
        , map News (s "actualites" </> newsidParser)
        , map Us (s "presentation")
        , map Edition (s "editions" </> eidParser)
        , map Candidates (s "candidats" </> eidParser)
        , map Encounters (s "rencontres")
        , map Movie (s "film" </> UrlParser.int)
        , map Article (s "articles" </> UrlParser.int)
        , map Participate (s "questions" </> eidParser)
        ]


type Origin
    = OriginUrl String


locationOrigin : Location -> Origin
locationOrigin location =
    OriginUrl location.origin


parseLocation : Location -> Route
parseLocation location =
    case parseHash parser location of
        Just route ->
            route

        Nothing ->
            NotFound


href : Route -> Origin -> Attribute msg
href dest origin =
    Attr.href (routeStr dest origin)


modifyUrl : Route -> Origin -> Cmd msg
modifyUrl dest origin =
    Navigation.modifyUrl (routeStr dest origin)


interactDest : Interaction -> Origin -> Cmd msg
interactDest interaction origin =
    case interaction of
        Nominate eid ->
            modifyUrl (Participate eid) origin

        Reward eid ->
            modifyUrl (Participate eid) origin

        Ask eid ->
            modifyUrl (Participate eid) origin

        _ ->
            Cmd.none


routeStr : Route -> Origin -> String
routeStr dest (OriginUrl origin) =
    let
        path =
            case dest of
                NewsLatest ->
                    []

                NewsFeed ->
                    [ "actualites" ]

                News nid ->
                    [ "actualites", nidStr nid ]

                Us ->
                    [ "presentation" ]

                Edition eid ->
                    [ "editions", eidStr eid ]

                Candidates aid ->
                    [ "candidats", eidStr aid ]

                Encounters ->
                    [ "rencontres" ]

                Movie i ->
                    [ "film", toString i ]

                Article i ->
                    [ "articles", toString i ]

                Participate eid ->
                    [ "questions", eidStr eid ]

                NotFound ->
                    []
    in
        origin ++ "/#/" ++ String.join "/" path
