module Data.Movie
    exposing
        ( Movie
        , movieDecoder
        )

import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (date)
import Json.Decode.Pipeline exposing (decode, required)


type alias Movie =
    { id : Int
    , title : String
    , desc : String
    , date : Date
    , img : String
    }



-- SERIALIZERS --


movieDecoder : Decoder Movie
movieDecoder =
    decode Movie
        |> required "id" Decode.int
        |> required "title" Decode.string
        |> required "desc" Decode.string
        |> required "date" date
        |> required "img" Decode.string
