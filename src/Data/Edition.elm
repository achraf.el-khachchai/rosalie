module Data.Edition
    exposing
        ( Edition
        , EditionId
        , Encounter
        , Interaction(..)
        , decoder
        , editionInteract
        , editionsDecoder
        , eidDecoder
        , eidInt
        , eidParser
        , eidStr
        , encountersDecoder
        , interactStr
        , interactTitle
        )

import Data.Movie exposing (Movie, movieDecoder)
import Data.Util exposing (strId)
import Date exposing (Date, toTime)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (date)
import Json.Decode.Pipeline exposing (decode, required)
import UrlParser


type alias Edition =
    { id : EditionId
    , name : String
    , localisation : String
    , start : Date
    , nomination : Date
    , rewarding : Date
    , end : Date
    }


type alias Encounter =
    { edition : Edition
    , movies : List Movie
    }



-- IDENTIFIERS--


type EditionId
    = EditionId Int


eidInt : EditionId -> Int
eidInt (EditionId eid) =
    eid


eidStr : EditionId -> String
eidStr (EditionId eid) =
    toString eid


eidParser : UrlParser.Parser (EditionId -> a) a
eidParser =
    UrlParser.custom "EDITIONID" strEid



-- SERIALIZERS --


editionsDecoder : Decoder (List Edition)
editionsDecoder =
    Decode.list decoder


decoder : Decoder Edition
decoder =
    decode Edition
        |> required "id" eidDecoder
        |> required "name" Decode.string
        |> required "localisation" Decode.string
        |> required "start_date" date
        |> required "nomination_date" date
        |> required "rewarding_date" date
        |> required "end_date" date


encountersDecoder : Decoder (List Encounter)
encountersDecoder =
    Decode.list encounterDecoder


encounterDecoder : Decoder Encounter
encounterDecoder =
    decode Encounter
        |> required "edition" decoder
        |> required "movies" (Decode.list movieDecoder)


eidDecoder : Decoder EditionId
eidDecoder =
    Decode.map EditionId Decode.int



-- HELPERS --


type Interaction
    = Over EditionId
    | Starting Date
    | Nominate EditionId
    | Reward EditionId
    | Ask EditionId


editionInteract : Edition -> Date -> Interaction
editionInteract edition now =
    let
        t =
            toTime now

        start =
            toTime edition.start

        end =
            toTime edition.end

        nominate =
            toTime edition.nomination

        reward =
            toTime edition.rewarding

        interaction =
            if t > end then
                Over edition.id
            else if t < start then
                Starting edition.start
            else if t < nominate then
                Nominate edition.id
            else if t < reward then
                Reward edition.id
            else
                Ask edition.id
    in
    interaction


interactTitle : Interaction -> String
interactTitle interaction =
    case interaction of
        Over _ ->
            "Palmarès "

        Starting _ ->
            "À venir "

        Nominate eid ->
            "Candidatures "

        Reward eid ->
            "Nominations "

        Ask eid ->
            "Palmarès "


interactStr : Interaction -> String
interactStr interaction =
    case interaction of
        Nominate eid ->
            "Phase 1 - Sélectionnez les nominés"

        Reward eid ->
            "Phase 2 - Votez pour le gagant"

        Ask eid ->
            "Posez des questions aux lauréats"

        _ ->
            ""



-- INTERNALS --


strEid : String -> Result String EditionId
strEid =
    strId EditionId
