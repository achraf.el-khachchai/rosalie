module Data.Award
    exposing
        ( Award
        , AwardId
        , aidParser
        , aidStr
        , awardWinners
        , awardsDecoder
        , awardsWinnersIds
        , decoder
        )

import Data.Application as AppD exposing (App, AppId, appnameid)
import Data.Edition exposing (EditionId, eidDecoder)
import Data.Util exposing (strId)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (decode, optional, required)
import UrlParser


type alias Award =
    { id : AwardId
    , name : String
    , desc : String
    , candidates : List App
    , nominees : List App
    , winners : List App
    }



-- IDENTIFIERS --


type AwardId
    = AwardId Int


aidStr : AwardId -> String
aidStr (AwardId aid) =
    toString aid


aidParser : UrlParser.Parser (AwardId -> a) a
aidParser =
    UrlParser.custom "AWARDID" strAid


awardWinners : Award -> List ( String, App )
awardWinners award =
    List.map ((,) award.name) award.winners


awardsWinnersIds : List Award -> List ( String, AppId )
awardsWinnersIds awards =
    awards
        |> List.map awardWinners
        |> List.concat
        |> List.map appnameid



-- SERIALIZERS--


decoder : Decoder Award
decoder =
    decode Award
        |> required "id" (Decode.map AwardId Decode.int)
        |> required "categorie_name" Decode.string
        |> required "categorie_desc" Decode.string
        |> optional "candidates" AppD.appsDecoder []
        |> optional "nominees" AppD.appsDecoder []
        |> optional "winners" AppD.appsDecoder []


awardsDecoder : Decoder (List Award)
awardsDecoder =
    Decode.list decoder



-- INTERNALS --


strAid : String -> Result String AwardId
strAid =
    strId AwardId
