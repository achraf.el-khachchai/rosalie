module Data.Participate
    exposing
        ( Error
        , Field(..)
        , Form
        , Question
        , Validity(..)
        , clean
        , decodeResponse
        , encode
        , newAsk
        , questionAsk
        , questionCheck
        , questionSelect
        )

import Data.Application exposing (App, AppId, appidInt, appname, awappname)
import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)
import Util exposing ((=>))


type alias Form r =
    { r
        | mail : String
        , questions : Dict String Question
        , token : Maybe String
    }



-- SERIALIZATION --


encode : Form r -> Value
encode form =
    let
        questions =
            filterMapQuestions form.questions
    in
    Encode.object
        [ "mail" => Encode.string form.mail
        , "token" => Encode.string (Maybe.withDefault "" form.token)
        , "questions" => Encode.list (List.map encodeQuestion questions)
        ]


decodeResponse : Decoder Bool
decodeResponse =
    Decode.field "created" Decode.bool



-- IDENTIFIERS --


type alias Question =
    { choice : Maybe AppId
    , ask : String
    , check : Validity
    }


newAsk : Int -> List ( String, App ) -> Dict String AppId -> ( String, Question )
newAsk i apps appsids =
    let
        key =
            "Ask" ++ toString i

        choice =
            case apps of
                [] ->
                    Nothing

                awapp :: others ->
                    Dict.get (awappname awapp) appsids
    in
    ( key, Question choice "" None )


questionAsk : String -> Maybe Question -> Maybe Question
questionAsk ask maybe =
    case maybe of
        Just askr ->
            Just { askr | ask = ask }

        Nothing ->
            Nothing


questionSelect : Maybe AppId -> Maybe Question -> Maybe Question
questionSelect choice maybe =
    case maybe of
        Just askr ->
            Just { askr | choice = choice }

        Nothing ->
            Nothing


questionCheck : String -> Question -> Question
questionCheck key question =
    let
        check =
            case String.isEmpty question.ask of
                True ->
                    Wrong "Ne soyez pas timide, il faut poser une question !"

                False ->
                    Right
    in
    { question | check = check }


type Validity
    = None
    | Right
    | Wrong String


type alias Error =
    { field : Field
    , err : String
    }


type Field
    = MailField
    | AskField
    | Recaptcha


clean : List Error -> Field -> Validity
clean errs field =
    let
        filtered =
            List.filter (\err -> err.field == field) errs
    in
    case filtered of
        [] ->
            Right

        err :: others ->
            Wrong err.err



-- INTERNALS --


encodeQuestion : ( AppId, String ) -> Value
encodeQuestion ( appid, ask ) =
    Encode.object
        [ "application_id" => Encode.int (appidInt appid)
        , "question" => Encode.string ask
        ]


filterMapQuestions : Dict String Question -> List ( AppId, String )
filterMapQuestions dict =
    let
        questions =
            Dict.values dict
    in
    List.concat (List.map questionFilter questions)


questionFilter : Question -> List ( AppId, String )
questionFilter question =
    case question.choice of
        Just appid ->
            [ ( appid, question.ask ) ]

        Nothing ->
            []
