module Data.Article
    exposing
        ( Article
        , articleDecoder
        , articlesDecoder
        )

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (decode, required)


type alias Article =
    { id : Int
    , title : String
    , content : String
    }



-- SERIALIZERS --


articlesDecoder : Decoder (List Article)
articlesDecoder =
    Decode.list articleDecoder


articleDecoder : Decoder Article
articleDecoder =
    decode Article
        |> required "id" Decode.int
        |> required "title" Decode.string
        |> required "content" Decode.string
