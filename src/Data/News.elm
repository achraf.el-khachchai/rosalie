module Data.News
    exposing
        ( News
        , NewsId
        , decoder
        , feedDecoder
        , newsidParser
        , nidStr
        , orderNews
        )

import Data.Util exposing (strId)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (date)
import Json.Decode.Pipeline exposing (decode, required)
import UrlParser


type alias News =
    { id : NewsId
    , title : String
    , author : String
    , pub : Date
    , update : Date
    , img : String
    , summary : String
    , content : String
    }


orderNews : List News -> List News
orderNews news =
    news
        |> List.sortBy (.pub >> Date.toTime)
        |> List.reverse



-- IDENTIFIERS --


type NewsId
    = NewsId Int


nidStr : NewsId -> String
nidStr (NewsId nid) =
    toString nid


newsidParser : UrlParser.Parser (NewsId -> a) a
newsidParser =
    UrlParser.custom "NEWSID" strNid



-- SERIALIZERS --


feedDecoder : Decoder (List News)
feedDecoder =
    Decode.list decoder


decoder : Decoder News
decoder =
    decode News
        |> required "id" (Decode.map NewsId Decode.int)
        |> required "title" Decode.string
        |> required "author" Decode.string
        |> required "publication_date" date
        |> required "update_date" date
        |> required "img_src" Decode.string
        |> required "summary" Decode.string
        |> required "content" Decode.string



-- INTERNALS --


strNid : String -> Result String NewsId
strNid =
    strId NewsId
