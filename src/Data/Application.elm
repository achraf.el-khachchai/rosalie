module Data.Application
    exposing
        ( App
        , AppId
        , Person
        , appidInt
        , appidParser
        , appidStr
        , appname
        , awappname
        , appnameid
        , appsDecoder
        , decoder
        , fullname
        )

import Data.Movie exposing (Movie, movieDecoder)
import Data.Util exposing (strId)
import Date exposing (Date)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra exposing (date)
import Json.Decode.Pipeline exposing (decode, required)
import UrlParser


type alias App =
    { id : AppId
    , movie : Movie
    , person : Person
    }


type alias Person =
    { id : Int
    , firstname : String
    , lastname : String
    , birthdate : Date
    , desc : String
    , img : String
    }


appname : App -> String
appname app =
    fullname app.person ++ " - " ++ app.movie.title


awappname : ( String, App ) -> String
awappname ( awardname, app ) =
    appname app ++ " - " ++ awardname


appnameid : ( String, App ) -> ( String, AppId )
appnameid ( awardname, app ) =
    ( awappname (awardname, app), app.id )


fullname : Person -> String
fullname person =
    person.firstname ++ " " ++ person.lastname



-- IDENTIFIERS --


type AppId
    = AppId Int


appidParser : UrlParser.Parser (AppId -> a) a
appidParser =
    UrlParser.custom "APPID" strAppid


appidInt : AppId -> Int
appidInt (AppId appid) =
    appid


appidStr : AppId -> String
appidStr (AppId appid) =
    toString appid



-- SERIALIZERS --


appsDecoder : Decoder (List App)
appsDecoder =
    Decode.list decoder


decoder : Decoder App
decoder =
    decode App
        |> required "id" (Decode.map AppId Decode.int)
        |> required "movie" movieDecoder
        |> required "person" personDecoder



-- INTERNALS --


strAppid : String -> Result String AppId
strAppid =
    strId AppId


personDecoder : Decoder Person
personDecoder =
    decode Person
        |> required "id" Decode.int
        |> required "firstname" Decode.string
        |> required "lastname" Decode.string
        |> required "birthdate" date
        |> required "desc" Decode.string
        |> required "img" Decode.string
